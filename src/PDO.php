<?php

namespace mindplay\pdoh;

use PDO as the_other_PDO;
use RuntimeException;

class PDO extends the_other_PDO
{
    /**
     * @var string
     */
    protected $dsn;

    /**
     * @var string|null
     */
    protected $username;

    /**
     * @var string|null
     */
    protected $password;

    /**
     * @var array
     */
    protected $options;

    /**
     * @var bool
     */
    protected $disconnected = true;

    /**
     * @var array
     */
    protected $attributes = array();

    /**
     * Creates a PDO instance representing a connection to a database
     *
     * @link http://php.net/manual/en/pdo.construct.php
     *
     * @param string $dsn
     * @param string $username
     * @param string $password
     * @param array $options
     */
    public function __construct ($dsn, $username = null, $password = null, $options = array())
    {
        $this->dsn = $dsn;
        $this->username = $username;
        $this->password = $password;
        $this->options = $options;
    }

    /**
     * Internally connect to the database
     */
    protected function connect()
    {
        if ($this->disconnected) {
            parent::__construct($this->dsn, $this->username, $this->password, $this->options);

            $this->disconnected = false;
        }
    }

    public function prepare($statement, array $driver_options = array())
    {
        if ($this->disconnected) {
            $this->connect();
        }

        return parent::prepare($statement, $driver_options);
    }

    public function beginTransaction ()
    {
        if ($this->disconnected) {
            $this->connect();
        }

        return parent::beginTransaction();
    }

    public function inTransaction ()
    {
        if ($this->disconnected) {
            return false;
        }

        return parent::inTransaction();
    }

    public function setAttribute ($attribute, $value)
    {
        if ($this->disconnected) {
            $this->attributes[$attribute] = $value;

            return true; // assume success (we'll throw later, if there's an error)
        } else {
            return parent::setAttribute($attribute, $value);
        }
    }

    public function exec ($statement)
    {
        if ($this->disconnected) {
            $this->connect();
        }

        return parent::exec($statement);
    }

    public function query ($statement, $mode = PDO::ATTR_DEFAULT_FETCH_MODE, $arg3 = null)
    {
        if ($this->disconnected) {
            $this->connect();
        }

        return parent::query($statement, $mode, $arg3);
    }

    public function lastInsertId ($name = null)
    {
        if ($this->disconnected) {
            throw new RuntimeException("this PDO instance is disconnected");
        }

        return parent::lastInsertId($name);
    }

    public function errorCode ()
    {
        if ($this->disconnected) {
            return null; // as per documentation: returns NULL if no query has been executed
        }

        return parent::errorCode();
    }

    public function getAttribute ($attribute)
    {
        if ($this->disconnected) {
            return isset($this->attributes[$attribute])
                ? $this->attributes[$attribute]
                : null; // as per documentation: returns NULL if no attribute has been set
        }

        return parent::getAttribute($attribute);
    }
}
